;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \section{Procedures}

;;> \procedure{(gensym prefix)\br{}
;;> (gensym)}

;;> Returns a symbol that is different to all symbols that
;;> were previously returned by this procedure.
;;> \var{Prefix} is a symbol used as a prefix to construct the unique
;;> symbol.  If \var{prefix} is omitted, it defaults to the symbol
;;> \scheme{'g}.

(define gensym
  (case-lambda
    (() (gensym 'g))
    ((prefix)
     (let ((count *counter*))
       (set! *counter* (+ 1 *counter*))
       (string->symbol (string-append (symbol->string prefix)
                                      "."
                                      (number->string count))))))) 

;;;; Global variables

(define *counter* 0)
